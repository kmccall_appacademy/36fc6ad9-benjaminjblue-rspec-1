def translate(str)
    punctuations = {'?' => true, '!' => true, '.' => true, ',' => true, ':' => true, ';' => true}
    str.split(' ').map{|word|
        punctuation = (punctuations[word[-1]] ? word[-1] : '')
        word = word[0..-2] if punctuation != ''

        capital = word.capitalize == word
        word = (capital ? word.downcase : word)

        vowel_1 = word.index(/[aeiou]/)
        if vowel_1 != 0 && word[vowel_1 - 1] == 'q'
            vowel_1 = word.index(/[aeiou]/, vowel_1 + 1)
        end

        (word = word[vowel_1..-1] + word[0...vowel_1]) if vowel_1 != 0

        (capital ? word.capitalize : word) + 'ay' + punctuation
    }.join(' ')
end