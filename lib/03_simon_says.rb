def echo(str)
    str
end

def shout(str)
    str.upcase
end

def repeat(str, times = 2)
    (str + ' ') * (times - 1) + str
end

def start_of_word(str, len)
    str[0,len]
end

def first_word(str)
    i = str.index(' ')
    i ? str[0,i] : str
end

def titleize(str)
    short_words = ['a', 'an', 'and', 'the', 'over']
    .reduce({}){|hash, word|
        hash[word] = true
        hash
    }
    str = str.split(' ')
    str[0].capitalize!
    str.map{|word| short_words[word] ? word : word.capitalize}.join(' ')
end