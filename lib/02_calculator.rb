def add(a,b)
    a + b
end

def subtract(a,b)
    a - b
end

def sum(nums)
    nums == [] ? 0 : nums.reduce(:+)
end

def multiply(*nums)
    nums.reduce(1, :*)
end

def power(a,b)
    a ** b
end

def factorial(n)
    (1..n).reduce(1, :*)
end